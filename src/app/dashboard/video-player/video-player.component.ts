import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap, filter } from 'rxjs/operators';

import { Video } from 'src/app/app-types';
import { VideoDataService } from '../../video-data.service';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css']
})
export class VideoPlayerComponent implements OnInit {
  video: Observable<Video>;

  constructor(route: ActivatedRoute, videoSvc: VideoDataService) {
    this.video = route.queryParams.pipe(
      map(params => params['videoId']),
      filter(id => !!id),
      switchMap(id => videoSvc.loadSingleVideo(id))
    );
  }

  ngOnInit() {
  }

}
